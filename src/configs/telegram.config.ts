import { ConfigService } from '@nestjs/config';
import { ITelegramOptions } from 'src/telegram/telegram.interface';

export const getTelegramConfig = (
	configService: ConfigService,
): ITelegramOptions => {
	const token = configService.get('TELEGRAM_TOKEN');
	const chatId = configService.get('CHAT_ID') ?? '';

	if (!token) {
		throw new Error('TELEGRAM_TOKEN не задан');
	}
	return {
		chatId,
		token,
	};
};
