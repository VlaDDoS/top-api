import { HttpService } from '@nestjs/axios';
import { Inject, Injectable, Logger } from '@nestjs/common';
import {
	API_URL,
	CLUSTER_FIND_ERROR,
	HH_MODULE_OPTIONS,
	SALARY_CLUSTER_ID,
} from './hh.constants';
import { IHhOptions } from './hh.interface';
import { firstValueFrom } from 'rxjs';
import { HhResponse } from './hh.models';
import { HHData } from 'src/top-page/top-page.model';

@Injectable()
export class HhService {
	private options: IHhOptions;

	constructor(
		private readonly httpService: HttpService,
		@Inject(HH_MODULE_OPTIONS) options: IHhOptions,
	) {
		this.options = options;
	}

	async getData(text: string) {
		try {
			const { data } = await firstValueFrom(
				this.httpService.get<HhResponse>(API_URL.vacancies, {
					params: {
						text,
						clusters: true,
					},
					headers: {
						'User-Agent': 'TopApi/1.0 (mega.nozhkin@pm.me)',
						Authorization: `Bearer ${this.options.token}`,
					},
				}),
			);

			return this.parseData(data);
		} catch (e) {
			Logger.error(e);
		}
	}

	private parseData(data: HhResponse): HHData {
		const salaryCluster = data.clusters.find(
			(cluster) => cluster.id === SALARY_CLUSTER_ID,
		);

		if (!salaryCluster) {
			throw new Error(CLUSTER_FIND_ERROR);
		}

		const juniorSalary = this.getSalaryFromString(
			salaryCluster.items[1].name,
		);
		const middleSalary = this.getSalaryFromString(
			salaryCluster.items[Math.ceil(salaryCluster.items.length / 2)].name,
		);
		const seniorSalary = this.getSalaryFromString(
			salaryCluster.items.at(-1).name,
		);

		return {
			count: data.found,
			juniorSalary,
			middleSalary,
			seniorSalary,
			updatedAt: new Date(),
		};
	}

	private getSalaryFromString(salary: string): number {
		const numberRegExp = /(\d+)/g;
		const result = salary.match(numberRegExp);

		if (!result) {
			return 0;
		}

		return Number(result);
	}
}
