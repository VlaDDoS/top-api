import { DynamicModule, Global, Module, Provider } from '@nestjs/common';
import { HhService } from './hh.service';
import { HH_MODULE_OPTIONS } from './hh.constants';
import { IHhModuleAsyncOptions } from './hh.interface';
import { HttpModule } from '@nestjs/axios';

@Global()
@Module({})
export class HhModule {
	static forRootAsync(options: IHhModuleAsyncOptions): DynamicModule {
		const asyncOptions = this.createAsyncOptionsProvider(options);

		return {
			module: HhModule,
			imports: options.imports,
			providers: [HhService, asyncOptions],
			exports: [HhService],
		};
	}

	private static createAsyncOptionsProvider(
		options: IHhModuleAsyncOptions,
	): Provider {
		return {
			provide: HH_MODULE_OPTIONS,
			useFactory: async (...args: any[]) => {
				const config = await options.useFactory(...args);
				return config;
			},
			inject: options.inject || [],
		};
	}
}
